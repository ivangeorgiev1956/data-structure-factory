# Data structure factory

Node.js module that provides factory functions regarding common data structures.

```javascript
const DataStructureFactory = require("data-structure-factory");

const stack = DataStructureFactory.createStack();

stack.push(5);
stack.push(10);
stack.push(15);

stack.print();

let sum = 0;
while (!stack.isEmpty()) {
    sum += stack.pop();
}
console.log("The sum of the stack is " + sum);
```