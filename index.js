module.exports.createStack = function createStack() {
    const data = [];
    return {
        pop: function () {
            return data.pop();
        },
        push: function (element) {
            data.push(element);
        },
        isEmpty: function () {
            return data.length === 0;
        },
        print: function () {
            for (let i = data.length - 1; i >= 0; --i) {
                console.log(data[i]);
            }
        }
    };
};